From the primary yoga philosophies to barre, pilates, and aerial, many physical modalities are incorporated into our full, seven day schedule. HARD yoga also offers teacher trainings, reiki healing, a retail boutique, lounge, and special events for our thriving community. 

Address: 6781 Warner Ave, Huntington Beach, CA 92647, USA

Phone: 714-375-3030

